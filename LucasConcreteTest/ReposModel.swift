//
//  RepoModel.swift
//  LucasConcreteTest
//
//  Created by Lucas Moraes on 05/07/17.
//  Copyright © 2017 Lucas Moraes. All rights reserved.
//

import Foundation
import ObjectMapper

// Model Class for the Repos
// modelos para os repositórios
class ReposModel: Mappable {
        
    var nameOwner: String
    var fullNameOwner: String
    
    var repoName: String
    var repoDescrip: String
    
    var avatarURL: String
    var reposPullURL: String
    
    var nStars: Int
    var nForks: Int
    
    required init(map:Map) {
        
        nameOwner = ""
        fullNameOwner = ""
        
        repoName = ""
        repoDescrip = ""
        
        avatarURL = ""
        reposPullURL = ""

        nStars = 0
        nForks = 0
    }
    
    func mapping(map: Map) {
        
        self.nameOwner <- map["owner.login"]
        self.fullNameOwner <- map["full_name"]
        
        self.repoName <- map["name"]
        self.repoDescrip <- map["description"]
        
        self.avatarURL <- map["owner.avatar_url"]
        self.reposPullURL <- map["pulls_url"]
        
        self.nStars <- map["stargazers_count"]
        self.nForks <- map["forks_count"]
    } 
    
}

