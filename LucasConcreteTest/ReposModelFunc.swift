//
//  ModelFunc.swift
//  LucasConcreteTest
//
//  Created by Lucas Moraes on 05/07/17.
//  Copyright © 2017 Lucas Moraes. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire

// Classe para baixar dados dos repositórios
// consumindo o "ReposModel" para o mepeamento dos objetos
typealias DownloadReposCompleted = () -> ()
class ReposModelFunc  {
    
    var model = [ReposModel]()//Repositório modelo
    
    var nPages = 1 // number of pages for endless scroll/número de páginas para o scroll infinito

    func downloadRepos(completed: @escaping DownloadReposCompleted) {
        let reposURL = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=\(nPages)"
        
        Alamofire.request(reposURL).responseJSON { response in
            let result = response.result
            if result.isSuccess {
                if let dict = result.value as? Dictionary<String,AnyObject> {
                    if let items = dict["items"] as? [Dictionary<String,AnyObject>] {
                        for item in items {
                            let map = Mapper<ReposModel>().map(JSON: item)
                            self.model.append(map!)
                        }
                    }
                }
            }
            completed()
        }
        
    }
    
   
    
}




