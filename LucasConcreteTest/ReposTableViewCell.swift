//
//  TableViewCell.swift
//  LucasConcreteTest
//
//  Created by Lucas Moraes on 06/07/17.
//  Copyright © 2017 Lucas Moraes. All rights reserved.
//

import UIKit

class ReposTableViewCell: UITableViewCell {
    
    // Label's Outlet's
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var fullNameLabel: UILabel!
    
    @IBOutlet weak var repoLabel: UILabel!
    @IBOutlet weak var repoDescripLabel: UILabel!
    
    @IBOutlet weak var nForksLabel: UILabel!
    @IBOutlet weak var nStarsLabel: UILabel!
    
    // Image's Outlet's
    @IBOutlet weak var avatarImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        avatarImage.layer.borderWidth = 1
        avatarImage.layer.masksToBounds = false
        avatarImage.layer.borderColor = UIColor.white.cgColor
        avatarImage.layer.cornerRadius = 5
        avatarImage.clipsToBounds = true
    } 
}
