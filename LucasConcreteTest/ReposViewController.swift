//
//  ViewController.swift
//  LucasConcreteTest
//
//  Created by Lucas Moraes on 05/07/17.
//  Copyright © 2017 Lucas Moraes. All rights reserved.
//  

// The normal ViewController Class for 1° Viewpage, whith all repositories
// classe ViewController para a 1ª View com os Repositórios (Java)

import UIKit
import Alamofire
import ObjectMapper
import SDWebImage //BEST FRAMEWORK EVER

class ReposViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    let modelFunc = ReposModelFunc() // pulling the class function method
    var avatarImage = UIImageView()

    override func viewDidAppear(_ animated: Bool) {
    
        tableView.delegate = self
        tableView.dataSource = self
        
        updateNavBar()
        modelFunc.downloadRepos {self.fadeTableView()}
        
    }

    // TableView method's/funções da tableView ->
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.modelFunc.model.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        nextStep(indexP: indexPath)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return theCell(indexP: indexPath)
    }
    // scroll infinito 
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.modelFunc.model.count-1 {
            modelFunc.nPages += 1
            modelFunc.downloadRepos {
                self.tableView.reloadData()
            }
        }
    }
    // <- TableView method's
    
    // cell for tableView method/Tratamento para a célula
    func theCell(indexP: IndexPath) -> UITableViewCell {
        
        // Inheriting the cell class
        let cell = tableView.dequeueReusableCell(withIdentifier: "theCell", for: indexP) as! ReposTableViewCell
        
        let urlImage =  modelFunc.model[indexP.row].avatarURL
        let imageAvatar = cell.avatarImage as UIImageView
        
        imageAvatar.sd_setImage(with: URL(string: urlImage))
        
        cell.nameLabel.text = modelFunc.model[indexP.row].nameOwner
        cell.fullNameLabel.text = modelFunc.model[indexP.row].fullNameOwner
        
        cell.repoLabel.text = modelFunc.model[indexP.row].repoName
        cell.repoDescripLabel.text = modelFunc.model[indexP.row].repoDescrip
        
        cell.nStarsLabel.text = String(modelFunc.model[indexP.row].nStars)
        cell.nForksLabel.text = String(modelFunc.model[indexP.row].nForks)
        
        return cell
    }
    
    // update the navigation bar
    private func updateNavBar() {
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white,NSFontAttributeName: UIFont(name: "Menlo-Regular", size: 19.0)!]
    }
    
    // push PullRequest Viewcontroller with Data/chama o PullRequest Viewcontroller e manda a url para buscar PullRequests
    private func nextStep(indexP: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let repoName = modelFunc.model[indexP.row].repoName
        let nameOwner = modelFunc.model[indexP.row].nameOwner
        
        let vc = storyboard.instantiateViewController(withIdentifier: "PullsViewController") as! PullsViewController
        vc.repoPullsName = repoName
        let url = "https://api.github.com/repos/\(nameOwner)/\(repoName)/pulls"
        vc.reposURL = url
        
        
        print(modelFunc.model[indexP.row].reposPullURL)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // when the tableView is not empty, fade whith the results
    //quando a os dados estiverem baixados, aplica in fade in na tabela
    private func fadeTableView() {
        self.tableView.reloadData()
        UIView.animate(withDuration: 0.7) {
            self.spinner.stopAnimating()
            self.tableView.alpha = 1
        }
    }

}

