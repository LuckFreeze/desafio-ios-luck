//
//  PullsModelFunc.swift
//  LucasConcreteTest
//
//  Created by Lucas Moraes on 06/07/17.
//  Copyright © 2017 Lucas Moraes. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire

// Classe para baixar dados dos pulls request's
// consumindo o "PullsModel" para o mepeamento dos objetos
typealias DownloadPullsCompleted = () -> ()
class PullsModelFunc  {
    
    var model = [PullsModel]()
    
    var pullsURL = ""

    func downloadRepos(completed: @escaping DownloadPullsCompleted) {
        Alamofire.request(pullsURL).responseJSON { response in
            
            let result = response.result
            if let items = result.value as? [Dictionary<String,AnyObject>] {
                for item in items {
                    let map = Mapper<PullsModel>().map(JSON: item)
                    self.model.append(map!)
                }
            }
            print(result)
            completed()
        }
    }
} 

