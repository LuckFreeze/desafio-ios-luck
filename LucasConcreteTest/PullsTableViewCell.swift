//
//  PullsTableViewCell.swift
//  LucasConcreteTest
//
//  Created by Lucas Moraes on 06/07/17.
//  Copyright © 2017 Lucas Moraes. All rights reserved.
//

import UIKit

class PullsTableViewCell: UITableViewCell {

    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var repoLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descripLabel: UILabel!
    
    // Image Outlet
    @IBOutlet weak var avatarImage: UIImageView!
    
        override func awakeFromNib() {
            super.awakeFromNib()

            avatarImage.layer.borderWidth = 1
            avatarImage.layer.masksToBounds = false
            avatarImage.layer.borderColor = UIColor.clear.cgColor
            avatarImage.layer.cornerRadius = 5
            avatarImage.clipsToBounds = true
        }

}
