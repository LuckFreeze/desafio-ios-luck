//
//  PullsViewController.swift
//  LucasConcreteTest
//
//  Created by Lucas Moraes on 06/07/17.
//  Copyright © 2017 Lucas Moraes. All rights reserved.
//  

// ViewController com os Pull Requests selecionados anteriormente

import UIKit

class PullsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    var reposURL: String?
    var repoPullsName: String?
    let pullsModel = PullsModelFunc()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self

        updateMainUI()
        pullsModel.pullsURL = reposURL!
        pullsModel.downloadRepos {self.fadeTableView()}
        
    }
    
    // TableView method's/funções da tableView
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pullsModel.model.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        nextStep(indexP: indexPath)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return theCell(indexP: indexPath)
    }
    
    func theCell(indexP: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "theCell", for: indexP) as! PullsTableViewCell
        
        let urlImage =  pullsModel.model[indexP.row].avatarURL
        
        let imageAvatar = cell.avatarImage as UIImageView
        
        imageAvatar.sd_setImage(with: URL(string: urlImage))
        
        cell.nameLabel.text = pullsModel.model[indexP.row].nameOwner
        cell.descripLabel.text = pullsModel.model[indexP.row].pullDescrip
        cell.repoLabel.text = pullsModel.model[indexP.row].pullTitle
        cell.dateLabel.text = pullsModel.model[indexP.row].pullData

        return cell
    }
    
    // Título,Font,backButton do navigationController
    func updateMainUI() {
        self.navigationItem.title = "Pull Requests"
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white,NSFontAttributeName: UIFont(name: "Menlo-Regular", size: 19.0)!]
        if let topItem = self.navigationController?.navigationBar.topItem {
            topItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        }
    }
    
    // push Safari with pullurl/puxando Safari com a url
    private func nextStep(indexP: IndexPath) {
        
        let url = URL(string:"\(pullsModel.model[indexP.row].url)")
        UIApplication.shared.openURL(url!)
    }
    
    // when the tableView is not empty, fade whith the results
    //quando a os dados estiverem baixados, aplica in fade in na tabela
    private func fadeTableView() {
        self.tableView.reloadData()
        UIView.animate(withDuration: 0.7) {
            self.spinner.stopAnimating()
            self.tableView.alpha = 1
        }
    }
}
