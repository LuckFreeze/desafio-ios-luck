//
//  PullsModel.swift
//  LucasConcreteTest
//
//  Created by Lucas Moraes on 06/07/17.
//  Copyright © 2017 Lucas Moraes. All rights reserved.
//

import Foundation
import ObjectMapper

// Model Class for Pulls
// modelos para os pull request's
class PullsModel: Mappable {
    
    var nameOwner: String
    var avatarURL: String
    
    var url: String
    
    var pullTitle: String // Pull Name/título da PullRequest
    var pullData: String
    
    var pullDescrip: String // body Descrip/corpo do pull
  
    required init(map:Map) {
        nameOwner = ""
        avatarURL = ""
        url = ""
        pullTitle = ""
        pullData = ""
        pullDescrip = ""
    }
    
    func mapping(map: Map) {
        self.nameOwner <- map["user.login"]
        self.avatarURL <- map["user.avatar_url"]
        
        self.url <- map["html_url"]
        
        self.pullTitle <- map["title"]
        self.pullData <- map["created_at"]
        
        self.pullDescrip <- map["body"]

    }
}








